beautifulsoup4==4.9.3
Django==2.2.13
djangorestframework==3.12.2
psycopg2==2.8.6
python-dotenv==0.15.0
requests==2.25.1