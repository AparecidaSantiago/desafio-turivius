from django.urls import path
from . import views

urlpatterns = [
  path('', views.get_all, name= 'get'),
  path('update/<int:pk>/', views.update, name= 'update'),
  path('delete/<int:pk>/', views.delete, name= 'delete'),
]