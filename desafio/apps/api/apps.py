from django.apps import AppConfig


class CrawlerConfig(AppConfig):
    name = 'desafio.apps.api'
