from django.db import models

class Book(models.Model):
    title = models.CharField(max_length=100, null=True, blank=True)
    price = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)
    url = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(max_length=8192, null=True, blank=True)

    def __str__(self):
        return self.title
