import json
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import *
from .serializers import BookSerializer
from rest_framework import status
from django.core.exceptions import ObjectDoesNotExist

class BookViewSet(viewsets.ModelViewSet):

    serializer_class = BookSerializer
    queryset = Book.objects.all()

@api_view(["GET"])
@csrf_exempt
def get_all(request):
    """Método para obter todos os livros"""
    books = Book.objects.all()
    serializer = BookSerializer(books, many=True)
    return JsonResponse({'books': serializer.data}, safe=False, status=status.HTTP_200_OK)

@api_view(["PATCH", "PUT"])
@csrf_exempt
def update(request, pk):
    """Metodo para atuzalizaçao dos dados de um livro"""
    payload = json.loads(request.body)
    try:
        book = Book.objects.filter(id=pk)
        book.update(**payload)
        book = Book.objects.get(id=pk)
        serializer = BookSerializer(book)
        return JsonResponse({'book': serializer.data}, safe=False, status=status.HTTP_200_OK)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception:
        return JsonResponse({'error': 'Ocorreu um erro no servidor'}, safe=False,
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(["DELETE"])
@csrf_exempt
def delete(request, pk):
    """Metodo para exclusao de um livro"""
    try:
        book = Book.objects.get(pk=pk)
        book.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception:
        return JsonResponse({'error': 'Ocorreu um erro no servidor'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

