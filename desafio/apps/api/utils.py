from collections import namedtuple
import psycopg2


def insert(title, price, url, description):
    conn = psycopg2.connect("dbname='library' user='postgres' password='postgres' host='localhost' port='5432'")
    cursor = conn.cursor()
    cursor.execute("INSERT INTO api_book (title, price, url, description) VALUES (%s, %s, %s, %s);", (title, price, url, description))
    conn.commit()
    conn.close()