from bs4 import BeautifulSoup
from decimal import Decimal
import requests, re
from utils import insert

__BASE_URL__ = "http://books.toscrape.com/"

def parse(url):
    """Método para realizar formatação do response"""
    html = requests.get(url).content
    soup = BeautifulSoup(html, 'html.parser')

    return soup

def get_pages(category):
    soup = parse(__BASE_URL__)

    #Obtem o texto da tag side_categories
    categories = soup.find(class_='side_categories')
    #Lista das subcategorias de Book
    categories_list = categories.find('a', text=re.compile(category))
    #Url das categorias
    page_url =  __BASE_URL__+categories_list.get("href")
    #Url padrão da categoria Sequential Art
    url_category = page_url.replace("index.html", "")
    #Lista das urls das categorias
    urls_list = [page_url]

    while page_url is not None:
        soup = parse(page_url)
        try:
            # Proxima pagina da url base da subcategoria sequential art
            next = soup.select_one(".next a")
            page_url = url_category + str(next.get('href'))
            urls_list.append(page_url)
        except:
            page_url = None

    return urls_list

if __name__ == '__main__':
    #Url parametrizada
    urls_pages = get_pages("Sequential Art")

    for page in urls_pages:
        soup = parse(page)
        # Lista de livros de Sequential Art
        articles_list = soup.select(".product_pod h3 a")
        #Lista de preços dos livros
        prices_list = soup.select(".price_color")

        for article, price in zip(articles_list, prices_list):
            #Titulo individual do livro
            title = str(article.get("title"))
            #Preço individual do livro
            price = price.text.strip()
            #URL individual do livro
            url_product = requests.compat.urljoin(page, article.get("href"))

            soup = parse(url_product)
            #Descrição individual do livro
            description = soup.find('meta',attrs={'name':'description'}).get('content')
            #Método para criar a inserção de dados inicial no banco de dados
            #insert(title, Decimal(price[1: len(price)]), url_product, description.strip())





