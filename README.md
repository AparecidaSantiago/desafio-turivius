# API

## Como instalar

Antes de iniciar o desenvolvimento da aplicação, é necessário instalar as suas dependências.

```shell
python3 -m venv env # cria ambiente virtual
source env/bin/active  # ativa o ambiente virtual
pip install -r requirements.txt # instala os requisitos
```

## Configuração

Antes de iniciar o projeto é preciso criar uma base postgres com o nome "library" e em seguida configurar as variáveis de ambiente necessárias para um bom funcionamento. Crie uma cópia do arquivo `.env.sample` com o nome de `.env` e altere os valores de suas variáveis internas.

# Executando

Execute o comando abaixo para rodar as migrações no banco de dados:

```shell
python manage.py migrate
```

Carregue alguns valores de teste no seu banco de dados, rodando o seguinte comando:
```shell
python manage.py loaddata api desafio/apps/api/fixtures/initial_data.json
```

Execute o comando abaixo e um servidor web poderá ser acessado pelo endereço `http://127.0.0.1:8000/`.

```shell
python manage.py runserver
```

# Requisições

Para efetuar requisições, utilize como exemplo o modelo abaixo:

```shell
curl localhost:8000/api/
 ```
